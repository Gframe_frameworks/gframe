<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <link rel="stylesheet" type="text/css" href="<?php echo $root; ?>_theme/default/CSS/style.css">

    <title><?php echo $title; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<div class="header">
    <div class="logo">
        <a href="<?php echo $root; ?>"><img src="<?php echo $root; ?>images/system/gframe_logo.png" alt="Gframe Logo" title="Gframe Logo"></a>
    </div>
</div>