<?php
$cms = false;

if(defined('CMS'))
{
$cms = true;
}
require_once  __DIR__."/error.php";
//$error = new error();

require_once __DIR__."\RenderNgin.php";
//$render = new render();

require_once __DIR__."/images.php";

require_once __DIR__."/Response.php";
//$response = new response();
require_once __DIR__."/Connection.php";
//$db = new db();

require_once __DIR__ . "/Route.php";
$route = new route();
$router = $route->buildRoute($cms);
if($router)
{
    if(isset($_SESSION['debug']))
    {
        print_r($router);
    }

    $call = $router['call'];
    require $router['pageUrl'];
    $page = new $router['base'];
   $page->$call();
}

