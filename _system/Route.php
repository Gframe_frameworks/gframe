<?php

class route
{
    public function buildRoute($cms = false, $route = false)
    {
        if($route == false)
        {
            $route = $_SERVER['REQUEST_URI'];
        }
        //remove the leading and trailing slash
     $slug = trim($route, "/");
    if($cms)
    {
        $slug = str_replace('Gframe/','',$slug);
    }
     if(strstr($slug, "/"))
     {

         //slug has more slashes
         $routing = explode("/", $slug);
     }
     else
     {
         $routing[1] = 'home';
     }

     //routing[2] tells the system which file to open. if none is set it will point to index.php
     if(!array_key_exists(2,$routing)){
         $routing[2] = 'index';
     }
     //routing[3] tells the system which class to call. if none is set it will call index
    if(!array_key_exists(3,$routing)){
        $routing[3] = 'index';
    }
    //build file URI
    if($cms)
    {
        $fileUri = ROOT."./_control/_ngin/translator/".$routing[1]."/".$routing[2].".php";
    }
    else
    {
        $fileUri = ROOT."./_ngin/translator/".$routing[1]."/".$routing[2].".php";
    }


     //check if a translator map exist
        if(!is_file($fileUri))
        {

            $response = new response();
            $response->returnNotFound();
            return false;
            die();
        }
        else
        {
            //print_r($fileUri);
            $pageInfo = array(
                'base'      => $routing[1],
                'pageUrl'   => $fileUri,
                'call'      => $routing[3]
            );
            return $pageInfo;
        }

    }
}