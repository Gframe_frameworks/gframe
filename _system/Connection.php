<?php

class DB
{
    public function __construct()
    {
        $mysqli = new mysqli(DB_LOCATION, DB_USER,DB_PASS, DB_DATABASENAME);
        //check for errors
        if($mysqli->connect_error)
        {
            $error = new errorH();
            $error->error("Database connection error occurred.<br /> Error given:<br />".$mysqli->connect_errno, __FILE__, __LINE__);

        }
    }
}