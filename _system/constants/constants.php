<?php

//array of constant values within the configuration
$cons = array
(
    '_theme'         => 'default',
    '_control'     => '_control',
    'language'      => 'nl_NL',
    'currency'      => 'EUR',
    'tpl_directory' => ROOT.'/_theme/',
    'title'         => 'Gframe Page',
    'siteroot'      => '/Gframe/',
    'DB_location'   => 'localhost',
    'DB_user'       => 'root',
    'DB_pass'       =>  '',
    'DB_databasename'=> 'Gframe',
    'DB_prefix'     => 'GF_',
    'DB_postfix'    => '',
    'thumbnail_width'=> '90',
    'thumbnail_height'=>'90',
    'thumbail_bg'   => '255,255,255,1'
);

