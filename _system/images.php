<?php

class images
{
    public function thumbnail($width = THUMNAIL_WIDTH, $height = THUMBNAIL_HEIGHT, $src, $aspect = true, $rgba = THUMBAIL_BG, $folders = '')
    {
        $filename = substr($src, strpost('/'+1));
        $img = imagecreatefromjpeg($filename);
//or imagecreatefrompng,imagecreatefromgif,etc. depending on user's uploaded file extension

        $width = imagesx($img); //get width and height of original image
        $height = imagesy($img);

//determine which side is the longest to use in calculating length of the shorter side, since the longest will be the max size for whichever side is longest.
        if ($height > $width)
        {
            $ratio = $maxheight / $height;
            $newheight = $height;
            $newwidth = $width * $ratio;
        }
        else
        {
            $ratio = $maxwidth / $width;
            $newwidth = $width;
            $newheight = $height * $ratio;
        }

//create new image resource to hold the resized image
        $newimg = imagecreatetruecolor($newwidth,$newheight);

        $palsize = ImageColorsTotal($img);  //Get palette size for original image
        for ($i = 0; $i < $palsize; $i++) //Assign color palette to new image
        {
            $colors = ImageColorsForIndex($img, $i);
            ImageColorAllocate($newimg, $colors['red'], $colors['green'], $colors['blue']);
        }

//copy original image into new image at new size.
        imagecopyresized($newimg, $img, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

        imagejpeg($newimg,ROOT.'images/thumbnails/'.$folders); //$output file is the path/filename where you wish to save the file.
//Have to figure that one out yourself using whatever rules you want.  Can use imagegif() or imagepng() or whatever.
    }

    public function getUploadForm($target_id, $width = THUMNAIL_WIDTH, $height = THUMBNAIL_HEIGHT, $maxcount = 1)
    {
        $data['target_id'] = $target_id;
        $data['width'] = $width;
        $data['height'] = $height;
        $data['maxCount'] = $maxcount;
        //get form from template
        $render = new render();
        $form_part['form'] = $render->renderTpl('common/image_upload', $data);
        $form_part['script'] = "/_theme/".THEME."/JS/common/image.upload.js";
        return $form_part;
    }
}