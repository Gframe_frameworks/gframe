<?php

class render
{
    public function renderTpl($tpl, $data = array())
    {

        $dir = ROOT."/_theme/".THEME."/TPL/".$tpl.".tpl";
        if(defined('CMS'))
        {
            $dir = ROOT."/"._CONTROLE."/_theme/".THEME."/TPL/".$tpl.".tpl";
        }
       if(!is_file($dir))
        {
            $error = new errorH();
            return $error->error("Template file could not be found", $dir,__LINE__);
        }
       else
       {
           extract($data);

           ob_start();

           require($dir);

           return ob_get_clean();
           //return $dir;
       }
    }
}