<?php

class response
{
    public function outputPage($content = 0, $title = TITLE, $additionals = array()){
        $data = array();
        $data['title'] = $title;
        $data['root'] = SITEROOT;

        if(!empty($additionals))
        {
            foreach ($additionals as $key => $val)
            {
                if(strpos('script', $key))
                {
                    $data['scripts'][] = $val;
                }
                else
                {
                    $data[$key] = $val;
                }
            }
        }


        $render = new render();
        if($content === 0){
            $content = $render->renderTpl('/error/notFound');
        }

        $pageArray = array(
            'header' => $render->renderTpl('common/header', $data),
            'footer' => $render->renderTpl('common/footer', $data),
            'content' => $content
        );

        echo $render->renderTpl('common/page', $pageArray);

    }

    public function returnNotFound()
    {
        $this->outputPage();
    }
}