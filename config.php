<?php
//init globals

define('ROOT', __DIR__);
require_once __DIR__ . "/_system/constants/constants.php";
define('SITEROOT', $cons['siteroot']);
define('THEME', $cons['_theme']);
define('_CONTROLE', $cons['_control']);
define('LANGUAGE', $cons['language']);
define('CURRENCY', $cons['currency']);
define('TPLDIR', $cons['tpl_directory']);
define('TITLE', $cons['title']);
define('DB_USER', $cons['DB_user']);
define('DB_LOCATION', $cons['DB_location']);
define('DB_PASS', $cons['DB_pass']);
define('DB_DATABASENAME', $cons['DB_databasename']);
define('DB_PREFIX', $cons['DB_prefix']);
define('DB_POSTFIX', $cons['DB_postfix']);
define('THUMNAIL_WIDTH', $cons['thumbnail_width']);
define('THUMBNAIL_HEIGHT', $cons['thumbnail_height']);
define('THUMBAIL_BG', $cons['thumbail_bg']);