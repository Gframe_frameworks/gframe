##GFrame Lightweight frameworks Version

## version 0.01

## Although the framework is designed to be as flexible as possible and as such wants to post as little barriers as
## possible. The are some guidelines that can be handy to follow. Below are listed a few. Don't worry if you do not
## abide by them. They're just guidelines.

## DIRECTORY NAMING
## Think hard when you name a directory (or folder if you're on windows) as it might just break your project.
## depending on your server settings (and if you can modify them) A directory is either listed/denied entry (unless
## accessing a specific file) or captured my .htaccess. So having a directory with the same name as where you want to
## have pages in your project is a bad idea. For example, you have build a shop and you are using a uri liek this:
## 'Domain.com/products/A_very_cool_product/'. Now if you would add a directory Products to your root you could break
## your shop. So just don't. If you take a look at the Framework root you'll see that most critical directories have
## an underscore ( _ ) before the name. This is to isolated them from any accidentally named directories. It's a
## practice I encourage you to use. Of course it's up to you to make what you can with Gframe.
