<?php

class home
{
    public function index()
    {
        $render = new render();
        $response = new response();
        $image = new images();
        $upload_parts = $image->getUploadForm('Image');
        $data['uploadForm'] = $upload_parts['form'];
        $footer['scripts'][] = $upload_parts['script'];

        $home = $render->renderTpl('home/home', $data);
        $response->outputPage($home, 'A homepage', $footer);
    }
}

